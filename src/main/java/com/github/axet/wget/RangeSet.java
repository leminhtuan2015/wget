package com.github.axet.wget;

import java.util.ArrayList;
import java.util.Comparator;

public class RangeSet extends ArrayList<RangeSet.Range> { // BitSet / RoaringBitmap for long values
    public static class Range {
        public long beg; // inclusive [beg
        public long end; // inclusive end]

        public Range() {
        }

        public Range(Range r) {
            beg = r.beg;
            end = r.end;
        }

        public Range(long b, long e) {
            beg = b;
            end = e;
        }

        public long size() {
            return end - beg + 1;
        }

        public boolean contains(long v) {
            return beg >= v && v <= end;
        }

        public boolean lower(long beg) {
            return beg <= this.beg || beg <= this.end;
        }

        public void merge(Range r2) {
            if (r2.beg < beg)
                beg = r2.beg;
            if (r2.end > end)
                end = r2.end;
        }

        @Override
        public String toString() {
            return String.format("[%d, %d]", beg, end);
        }
    }

    public static class RangeSort implements Comparator<Range> {
        @Override
        public int compare(Range o1, Range o2) {
            return Long.valueOf(o1.beg).compareTo(Long.valueOf(o2.beg));
        }
    }

    public RangeSet() {
    }

    public RangeSet(long beg, long end) {
        add(beg, end);
    }

    public void add(long beg, long end) {
        add(new Range(beg, end));
    }

    public int lower(long beg) { // find beginning, lowest index to start from
        for (int i = 0; i < size(); i++) {
            Range r = get(i);
            if (r.lower(beg)) // assume list sorted
                return i;
        }
        return -1;
    }

    public int contains(long v) {
        for (int i = 0; i < size(); i++) {
            Range r = get(i);
            if (r.contains(v))
                return i;
        }
        return -1;
    }

    public void exclude(long beg, long end) { // inclusive [beg, end]
        int i = lower(beg);
        if (i == -1)
            return;
        for (; i < size();) {
            Range r = get(i);
            if (beg <= r.beg && r.end <= end) { // consume whole range
                remove(i);
                continue;
            }
            if (r.beg <= beg && end <= r.end) { // inside
                remove(i);
                Range left = new Range(r.beg, beg - 1);
                Range right = new Range(end + 1, r.end);
                if (right.size() > 0)
                    add(i, right);
                if (left.size() > 0)
                    add(i, left);
            }
            if (r.beg <= beg && beg <= r.end) // left unchanged
                r.end = beg - 1;
            if (r.beg <= end && end <= r.end) // right unchanged
                r.beg = end + 1;
            i++;
        }
    }

    public void include(long beg, long end) {
        exclude(beg, end);
        add(beg, end);
        sort();
    }

    public void merge() {
        for (int i = 0; i < size(); i++) {
            Range r1 = get(i);
            for (int k = i + 1; k < size(); k++) {
                Range r2 = get(k);
                if (r2.contains(r1.beg) || r2.contains(r1.end)) {
                    r1.merge(r2);
                    remove(k);
                    k = i; // restart, next + 1
                }
            }
        }
    }

    public void merge(long beg, long end) {
        exclude(beg, end);
        long b = beg;
        long e = end;
        int i = contains(beg - 1);
        if (i != -1) {
            b = get(i).beg;
            remove(i);
        }
        i = contains(end + 1);
        if (i != -1) {
            e = get(i).end;
            remove(i);
        }
        add(b, e);
        sort();
    }

    public void sort() {
        sort(new RangeSort());
    }
}
